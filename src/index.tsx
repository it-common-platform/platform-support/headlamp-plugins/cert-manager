import {
  DefaultDetailsViewSection,
  registerDetailsViewSectionsProcessor,
  registerSidebarEntry,
} from '@kinvolk/headlamp-plugin/lib';
import {
  SectionBox,
  SimpleTable,
  StatusLabel,
} from '@kinvolk/headlamp-plugin/lib/CommonComponents';
import { KubeObject } from '@kinvolk/headlamp-plugin/lib/K8s/cluster';
import { useEffect, useState } from 'react';
import { listCertificates } from './api/certificates';

export interface CertificateListProps {
  resource?: KubeObject;
}

export default function CertificateList(props: CertificateListProps) {
  const { resource } = props;
  const [certificates, setCertificates] = useState<Array<any> | null>(null);

  const namespace = resource.jsonData.metadata.name;

  useEffect(() => {
    if (!namespace) {
      // If namespace is not available, return early
      return;
    }

    listCertificates(namespace).then(response => {
      if (!response.items) {
        console.log('No response from fetchCertificates');
        setCertificates([]);
        return;
      }
      setCertificates(response.items);
    });
  }, []);

  return (
    <SectionBox title="cert-manager Certificates" textAlign="center" paddingTop={2}>
      <SimpleTable
        columns={[
          {
            label: 'Name',
            getter: certificate => certificate.metadata.name,
          },
          {
            label: 'Common Name',
            getter: certificate => certificate.spec.commonName,
          },
          {
            label: 'Alternate Names',
            getter: certificate => certificate.spec.dnsNames.join(', '),
          },
          {
            label: 'Status',
            getter: certificate => (
              <StatusLabel
                status={certificate.status.conditions[0].reason === 'Ready' ? 'success' : 'error'}
              >
                {certificate.status.conditions[0].reason}
              </StatusLabel>
            ),
          },
          {
            label: 'Expiration',
            getter: certificate => certificate.status.notAfter,
          },
        ]}
        data={certificates}
      />
    </SectionBox>
  );
}

// Sub-level sidebar entry, under cluster but without a cluster link.
registerSidebarEntry({
  parent: 'cluster',
  name: 'certificates',
  label: 'Certificates',
  url: '/customresources/certificates.cert-manager.io',
  icon: 'mdi:certificate',
});

registerDetailsViewSectionsProcessor(function addSubheaderSection(resource, sections) {
  // Ignore if there is no resource.
  if (!resource) {
    //console.log('No resource found');
    return sections;
  }

  //console.log('Resource kind is', resource.kind);
  //const namespace = resource.jsonData.metadata.name;
  //console.log('Namespace is:', namespace);
  //console.log('Resource is:', resource);

  if (resource.kind !== 'Namespace') {
    // Return early if we're not on a namespace page
    return sections;
  }

  // Check if we already have added our custom section (this function may be called multiple times).
  const customSectionId = 'add-certificates-to-namespace';
  if (sections.findIndex(section => section.id === customSectionId) !== -1) {
    return sections;
  }

  const detailsHeaderIdx = sections.findIndex(
    section => section.id === DefaultDetailsViewSection.MAIN_HEADER
  );
  // There is no header, so we do nothing.
  if (detailsHeaderIdx === -1) {
    return sections;
  }

  // We place our custom section after the header.
  sections.splice(detailsHeaderIdx + 7, 0, {
    id: 'add-certificates-to-namespace',
    section: <CertificateList resource={resource} />,
  });

  return sections;
});
