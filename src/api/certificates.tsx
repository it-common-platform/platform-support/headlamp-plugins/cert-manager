import { ApiProxy } from '@kinvolk/headlamp-plugin/lib';

const request = ApiProxy.request;
export const getHeadlampAPIHeaders = () => ({
  'X-HEADLAMP_BACKEND-TOKEN': new URLSearchParams(window.location.search).get('backendToken'),
});

export function listCertificates(namespace: string) {
  return request(`/apis/cert-manager.io/v1/namespaces/${namespace}/certificates`, {
    method: 'GET',
    headers: { ...getHeadlampAPIHeaders() },
  });
}
