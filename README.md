# cert-manager

This Headlamp plugin adds a `Certificates` link in the sidebar under `Cluster`. This links to the cert-manager CRD page. It also adds a `cert-manager Certificates` section under each namespace.

## Developing Headlamp plugins

For more information on developing Headlamp plugins, please refer to:

- [Platform Onboarding](https://onboarding.platform.it.vt.edu/development-environment/headlamp-plugins/)
- [Getting Started](https://headlamp.dev/docs/latest/development/plugins/), How to create a new Headlamp plugin.
- [API Reference](https://headlamp.dev/docs/latest/development/api/), API documentation for what you can do
- [UI Component Storybook](https://headlamp.dev/docs/latest/development/frontend/#storybook), pre-existing components you can use when creating your plugin.
- [Plugin Examples](https://github.com/headlamp-k8s/headlamp/tree/main/plugins/examples), Example plugins you can look at to see how it's done.
